package ru.novik.geomodule;

import org.apache.log4j.Logger;
import ru.novik.geomodule.exeptions.NotFoundException;

import java.io.Console;

/**
 * Created by nikita on 13.11.14.
 */
public class Test {

    static Logger log = Logger.getLogger(Test.class);

    public static void main(String[] args) {
        Console console = System.console();

        if (console == null) {
            System.err.println("No console.");
            System.exit(1);
        }

        GeoModule geoModule = new GeoModule();
        String s = console.readLine("Enter city: ");
        geoModule.setCityName(s);
        s = console.readLine("Enter street: ");
        geoModule.setStreetName(s);
        s = console.readLine("Enter house number: ");
        geoModule.setHouseNumber(s);

        try {
            System.out.println("[" + geoModule.getLatLon()[0] + "," + geoModule.getLatLon()[1] + "]");
        } catch (NotFoundException e) {
            log.error("latLon not found", e);
        }
    }
}
