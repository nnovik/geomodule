package ru.novik.geomodule.service;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by nikita on 13.11.14.
 */
public class GeoServiceImpl implements GeoService {

    static Logger log = Logger.getLogger(GeoServiceImpl.class);

    @Override
    public String searchGeoPointByAddress(String urlString) {
        log.debug("Create search request by URL = " + urlString);
        StringBuilder stringJSON = new StringBuilder();
        try {
            URL url = new URL(urlString);
            URLConnection connection = url.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                stringJSON.append(inputLine);
            in.close();
        } catch (IOException e) {
            log.error("Search failed! ", e);
        }
        log.debug("Search succeeded!");
        return stringJSON.toString();
    }

    @Override
    public String searchAddressByGeoPoint(String url) {
        return null;
    }
}