package ru.novik.geomodule.service;

import ru.novik.geomodule.entity.AddressData;

/**
 * Created by nikita on 13.11.14.
 */
public interface GeoService {

    String searchGeoPointByAddress(String urlString);

    String searchAddressByGeoPoint(String url);

}