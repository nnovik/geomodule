package ru.novik.geomodule;

import org.apache.log4j.Logger;
import ru.novik.geomodule.entity.AddressData;
import ru.novik.geomodule.entity.GeoPoint;
import ru.novik.geomodule.exeptions.NotFoundException;
import ru.novik.geomodule.util.AddressFinder;
import ru.novik.geomodule.util.BuildingAndStreetFinder;
import ru.novik.geomodule.util.StreetFinder;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nikita on 17.11.14.
 */
public class GeoModule {

    static Logger log = Logger.getLogger(GeoModule.class);

    private String cityName;
    private String streetName;
    private String streetNumber;
    private String houseNumber;
    private double[] latLon;

    public GeoModule() {
        this.streetNumber = "";
    }

    public GeoModule(String cityName, String streetName, String houseNumber) {
        this.cityName = cityName;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        log.debug("Received data cityName = " + cityName + ", streetName = " + streetName + ", houseNumber = " + houseNumber);
    }

    public double[] getLatLon() throws NotFoundException {
        if (latLon != null) return latLon;
        if (cityName == null || cityName.trim().isEmpty()) throw new NotFoundException();
        if (streetName == null || streetName.trim().isEmpty()) throw new NotFoundException();

        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(streetName);
        if (matcher.find()) {
            streetNumber = matcher.group();
            streetName = streetName.replaceFirst(streetNumber, "").trim();
            log.debug("Street name has a number. Setting streetName = " + streetName + ", streetNumber = " + streetNumber);
        }
        AddressData requestData = new AddressData(cityName, streetName, houseNumber, streetNumber);

        AddressFinder finder;
        if (requestData.getHouseNumber() != null) {
            finder = new BuildingAndStreetFinder(requestData);
        } else {
            finder = new StreetFinder(requestData);
        }

        try {
            GeoPoint geoPoint = finder.find();
            latLon = new double[]{geoPoint.getLat(), geoPoint.getLon()};
            return latLon;
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        throw new NotFoundException();
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
}
