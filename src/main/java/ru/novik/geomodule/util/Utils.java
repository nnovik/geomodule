package ru.novik.geomodule.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by nikita on 13.11.14.
 */
public class Utils {

    /**
     * Method counts and returns a List of nearest buildings.
     * An argument radius defines an amount of buildings which are on the given building right
     * and on the given building left.
     * I.e. radius = 3 and houseNumber = 8к1 returns {"8 к1", "8", "9", "7", "10", "6", "11", "5"}
     *
     * @return a List of Strings of nearest building numbers
     */
    public static List<String> nearestHouses(int radius, String houseNumber) {
        Integer intHouseNumber = parseHouseNumber(houseNumber);
        List<String> houseNumbers = new ArrayList<>();
        houseNumbers.add(houseNumber.replaceFirst("([0-9]+)[\\s]*", "$1 "));
        if (intHouseNumber != null) {
            if (!houseNumber.equals(intHouseNumber.toString())) houseNumbers.add(intHouseNumber.toString());
            for (int i = 1; i < radius; i++) {
                Integer number1 = intHouseNumber + i;
                houseNumbers.add(String.valueOf(number1));
                Integer number2 = intHouseNumber - i;
                if (number2 > 0) houseNumbers.add(String.valueOf(number2));
            }
        }
        return houseNumbers;
    }


    /**
     * Parse house number from the given string
     * @param houseNumber - string to be parsed
     * @return found number from the given string or null;
     */
    public static Integer parseHouseNumber(String houseNumber) {
        Pattern pattern = Pattern.compile("[0-9]+");
        Matcher matcher = pattern.matcher(houseNumber);
        String number = null;
        if (matcher.find()) {
            number = matcher.group();
        }
        if (number != null)
            return Integer.valueOf(number);
        else
            return null;
    }

}
