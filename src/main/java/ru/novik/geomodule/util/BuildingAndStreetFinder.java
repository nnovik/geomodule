package ru.novik.geomodule.util;

import org.apache.log4j.Logger;
import ru.novik.geomodule.entity.AddressData;
import ru.novik.geomodule.entity.GeoPoint;
import ru.novik.geomodule.exeptions.NotFoundException;

import java.util.List;


/**
 * Class uses when both the house number and street name are specified
 * Created by nikita on 15.11.14.
 */
public class BuildingAndStreetFinder extends AbstractFinder {

    static Logger log = Logger.getLogger(BuildingAndStreetFinder.class);

    public BuildingAndStreetFinder(AddressData requestData) {
        super(requestData);
    }

    /**
     * Returns geo point of found address. If found address is not satisfied the requested address data
     * then this method tries to find the nearest house number address.
     * @return GeoPoint object
     * @throws NotFoundException
     */
    @Override
    public GeoPoint find() throws NotFoundException {
        List<String> houses = Utils.nearestHouses(3, requestData.getHouseNumber());
        log.debug(houses.size() + " nearest houses counted");
        for (String house : houses) {
            requestData.setHouseNumber(house);
            try {
                return findHelper();
            } catch (NotFoundException e) {
                log.debug("house number " + house + " not found");
            }
        }
        requestData.setHouseNumber(null);
        return findHelper();
    }

    /**
     * Helps to find address data. This private method is using by find() method to help find
     * address data by changing house numbers. When the result matches to the conditionals it returns
     * to find method or NotFoundException is thrown;
     * @return GeoPoint object
     * @throws NotFoundException
     */
    private GeoPoint findHelper() throws NotFoundException {
        GeoPoint[] geoPoints = getGeoPoints();
        log.debug("Searching address in " + geoPoints.length + " found geo points");
        for (GeoPoint geoPoint : geoPoints) {
            AddressData addressData = geoPoint.getAddressData();
            if (addressData != null) {
                String sourceNumber = addressData.getHouseNumber();
                String sourceStreet = addressData.getStreetName();
                //check if found house number exactly equals requested (or nearest) house number
                //and if found street contains requested street
                //and if found street contains requested street number
                //or if there are no house numbers at all when the street middle coordinate is found
                if (sourceNumber != null && sourceStreet != null &&
                        sourceNumber.equals(requestData.getHouseNumber()) &&
                        sourceStreet.contains(requestData.getStreetName()) &&
                        sourceStreet.contains(requestData.getStreetNumber()) ||
                        sourceNumber == null && requestData.getHouseNumber() == null) {
                    return geoPoint;
                }
            }
        }
        throw new NotFoundException();
    }
}
