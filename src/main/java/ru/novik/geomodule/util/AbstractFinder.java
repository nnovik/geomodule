package ru.novik.geomodule.util;

import com.google.gson.Gson;
import ru.novik.geomodule.entity.AddressData;
import ru.novik.geomodule.entity.GeoPoint;
import ru.novik.geomodule.exeptions.NotFoundException;
import ru.novik.geomodule.service.GeoService;
import ru.novik.geomodule.service.GeoServiceImpl;

/**
 * The abstract class incapsulates data to
 * Created by nikita on 16.11.14.
 */
public class AbstractFinder implements AddressFinder {

    private static String BASE_URL = "http://nominatim.openstreetmap.org/search";
    protected AddressData requestData;
    private Gson gson;
    private GeoService geoService;
    private UrlConstructor urlConstructor;


    public AbstractFinder(AddressData requestData) {
        this.requestData = requestData;
        init();
    }

    private void init() {
        urlConstructor = new UrlConstructor(BASE_URL);
        gson = new Gson();
        geoService = new GeoServiceImpl();
    }


    @Override
    public GeoPoint find() throws NotFoundException {
        throw new NotFoundException();
    }

    protected GeoPoint[] getGeoPoints() {
        String stringJson = geoService.searchGeoPointByAddress(urlConstructor.getUrl(requestData));
        return gson.fromJson(stringJson, GeoPoint[].class);
    }



}
