package ru.novik.geomodule.util;

import ru.novik.geomodule.entity.GeoPoint;
import ru.novik.geomodule.exeptions.NotFoundException;

/**
 *
 * Created by nikita on 15.11.14.
 */
public interface AddressFinder {

    /**
     * Abstract method returns found geo point
     * @return GeoPoint object
     * @throws NotFoundException
     */
    public GeoPoint find() throws NotFoundException;

}
