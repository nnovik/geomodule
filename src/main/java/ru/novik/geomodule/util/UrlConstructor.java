package ru.novik.geomodule.util;

import ru.novik.geomodule.entity.AddressData;

/**
 * Created by nikita on 14.11.14.
 */
public class UrlConstructor {

    private String baseURL;

    public UrlConstructor(String baseURL) {
        this.baseURL = baseURL;
    }

    /**
     * Returns a URL which will use to search geo data from OpenStreetMap
     * @return URL string
     */
    public String getUrl(AddressData data) {
        StringBuilder builder = new StringBuilder(baseURL);
        builder.append(getQuery(data));
        builder.append("&format=json&addressdetails=1");
        return builder.toString();
    }

    /**
     * Concatenates parameters from entered address data for search request
     * @return query string
     */
    private String getQuery(AddressData data) {
        StringBuilder q = new StringBuilder("?street=");
        if (data.getHouseNumber() != null) {
            q.append(data.getHouseNumber().replaceFirst("([0-9]+[\\s]*)", "$1 ")).append(" ");
        }
        q.append(data.getStreetName());
        q.append("&city=").append(data.getCityName());
        return q.toString().replaceAll("[\\s]+", "+");
    }

}
