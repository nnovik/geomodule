package ru.novik.geomodule.util;

import ru.novik.geomodule.entity.AddressData;
import ru.novik.geomodule.entity.GeoPoint;
import ru.novik.geomodule.exeptions.NotFoundException;

/**
 * Class uses to find address data when house number is not specified
 * Created by nikita on 15.11.14.
 */
public class StreetFinder extends AbstractFinder {

    public StreetFinder(AddressData requestData) {
        super(requestData);
    }

    /**
     * Finds the street middle geo data
     * @return GeoPoint object
     * @throws NotFoundException
     */
    @Override
    public GeoPoint find() throws NotFoundException{
        GeoPoint[] geoPoints = getGeoPoints();
        for (GeoPoint geoPoint : geoPoints) {
            AddressData addressData = geoPoint.getAddressData();
            if (addressData != null) {
                String streetName = addressData.getStreetName();
                if (streetName.contains(requestData.getStreetName()))
                    return geoPoint;
            }
        }
        throw new NotFoundException();
    }
}
