package ru.novik.geomodule.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nikita on 13.11.14.
 */
public class AddressData {

    @SerializedName("house_number")
    private String houseNumber;

    @SerializedName("road")
    private String streetName;
    private String streetNumber;

    @SerializedName("city")
    private String cityName;

    @SerializedName("state_district")
    private String district;

    @SerializedName("state")
    private String state;

    @SerializedName("postcode")
    private String postcode;

    @SerializedName("country")
    private String country;

    @SerializedName("country_code")
    private String country_code;

    AddressData(){}

    public AddressData(String cityName, String streetName, String houseNumber, String streetNumber) {
        this.cityName = cityName;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.streetNumber = streetNumber;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }
}
